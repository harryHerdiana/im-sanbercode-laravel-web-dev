<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Form Pendaftaran</title>
  </head>
  <body>
    <form action="/welcome" method="POST">
        @csrf
      <h1>Buat Account Baru!</h1>
      <h3>Sign Up Form</h3>
      <p for="firstName">First Name:</p>
      <input type="text" name="firstName" required />
      <p for="lastName">Last Name:</p>
      <input type="text" name="lastName" required />
      <p>Gender:</p>
      <div>
        <input type="radio" value="male" name="radioINput" required />
        <label for="maleInput">Male</label>
      </div>
      <div>
        <input type="radio" value="female" name="radioINput" required />
        <label for="maleInput">Female</label>
      </div>
      <div>
        <input type="radio" value="other" name="radioINput" required />
        <label for="otherInput">Other</label>
      </div>
      <p>Nationality:</p>
      <select>
        <option value="indonesian">Indonesian</option>
        <option value="chinese">Chinese</option>
        <option value="other">Other</option>
      </select>
      <p>Language Spoken:</p>
      <div>
        <input type="checkbox" value="bahasa" name="checkboxInput" />
        <label for="bahasaInput">Bahasa Indonesia</label>
      </div>
      <div>
        <input type="checkbox" value="english" name="checkboxInput" />
        <label for="englishInput">English</label>
      </div>
      <div>
        <input type="checkbox" value="other" name="checkboxInput" />
        <label for="otherLanguageInput">Other</label>
      </div>
      <p>Bio :</p>
      <textarea cols="40" rows="10"></textarea>
      <br />
      <button type="submit">Sign Up</button>
    </form>
  </body>
</html>
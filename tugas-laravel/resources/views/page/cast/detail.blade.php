@extends('layout.master')

@section('title')
    Detail Cast
@endsection

@section('content')
  <h1>{{$cast->name}}</h1>
  <p><span>Age : </span>{{$cast->age}}</p>
  <p><span>Bio : </span>{{$cast->bio}}</p>
@endsection

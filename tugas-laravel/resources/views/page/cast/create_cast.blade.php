@extends('layout.master')

@section('title')
    Add Cast
@endsection

@section('content')
    <form method="POST" action="/cast">
        @csrf
        <div class="form-group">
            <label for="exampleInputEmail1">Cast Name</label>
            <input type="text" class="form-control @error('name')
                is-invalid
            @enderror"
                name="name">
        </div>
        @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="exampleInputEmail1">Cast Bio</label>
            <textarea name="bio" id="" cols="30" rows="10"
                class="form-control @error('bio')
            is-invalid
        @enderror"></textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="exampleInputEmail1">Cast Age</label>
            <input type="number" class="form-control @error('age')
                is-invalid
            @enderror"
                name="age">
        </div>
        @error('age')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection

@extends('layout.master')

@section('title')
    All Cast
@endsection

@section('content')
    <a href="/cast/create" class="btn btn-primary btn-sm">Add Cast</a>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">First</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key=>$item)
                <tr>
                    <th scope="row">{{ $key + 1 }}</th>
                    <td>{{ $item->name }}</td>
                    <td>
                        <form action="/cast/{{ $item->id }}" method="POST">
                            @csrf
                            @method('delete')
                            <a href="/cast/{{ $item->id }}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/cast/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Update</a>
                            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td>Cast data not found!</td>
                </tr>
            @endforelse


        </tbody>
    </table>
@endsection

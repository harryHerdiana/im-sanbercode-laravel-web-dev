<?php

namespace App\Http\Controllers;


class TableController extends Controller
{
    public function table()
    {
        return view('page.table');
    }

    public function data_table()
    {
        return view('page.data-table');
    }
}

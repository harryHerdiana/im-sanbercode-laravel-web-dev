<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('page.cast.create_cast');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:2',
            'bio' => 'min:20',
            'age' => 'required',
        ]);

        DB::table('cast')->insert([
            'name' => $request->input('name'),
            'bio' => $request->input('bio'),
            'age' => $request->input('age')
        ]);
        return redirect('/cast');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();
        return view('page.cast.index', ['cast' => $cast]);
    }

    public function detail($id)
    {
        $cast = DB::table('cast')->find($id);
        return view('page.cast.detail', ['cast' => $cast]);
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->find($id);
        return view('page.cast.edit', ['cast' => $cast]);
    }
    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required|min:2',
            'bio' => 'min:20',
            'age' => 'required',
        ]);

        DB::table('cast')
            ->where('id', $id)
            ->update(
                [
                    'name' => $request->input('name'), 'bio' => $request->input('bio'), 'age' => $request->input('age')
                ]
            );
        return redirect('/cast');
    }
    public function delete($id)
    {
        DB::table('cast')
            ->where('id', '=', $id)
            ->delete();
        return redirect('/cast');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('page.form');
    }

    public function signup(Request $request)
    {
        $firstName = $request->input('firstName');
        $lastName = $request->input('lastName');
        // return view('page.welcome', ['firstName'=>$firstName, 'lastName'=>$lastName]);
        return view('page.welcome', compact(["firstName", "lastName"]));

    }
}
Soal 1 Buat Database:

CREATE DATABASE myshop;

Soal 2 Buat Table:
table users:
CREATE TABLE users( id int PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT null, email varchar(255) NOT null, password varchar(255) NOT null );

table categories:
CREATE TABLE categories( id int PRIMARY KEY AUTO_INCREMENT, name varchar(255) );

table items:
create table items( id int AUTO_INCREMENT PRIMARY KEY, name varchar(255) NOT null, description varchar(255), price int(8) NOT null, stock int(8) NOT null, category_id int(11) NOT null, FOREIGN KEY (category_id) REFERENCES categories(id) );

Soal 3 Insert Database
users
INSERT INTO users(name,email,password) VALUES("John Doe", "john@doe.com", "john123");
INSERT INTO users(name,email,password) VALUES("Jane Doe", "jane@doe.com", "jenita123");

categories
INSERT INTO categories(name) VALUES("gadget"),("cloth"),("men"),("women"),("branded")

items
INSERT INTO items(name,description, price, stock, category_id) VALUES("Sumsang b50","hape keren dari merek sumsang", 4000000, 100, 1)
INSERT INTO items(name,description, price, stock, category_id) VALUES("Uniklooh","baju keren dari brand ternama", 500000, 50, 2)
INSERT INTO items(name,description, price, stock, category_id) VALUES("IMHO Watch","jam tangan anak yang jujur banget", 2000000, 10, 1),

Soal 4
4.a

SELECT id,name,email form users;

4.b
SELECT * from items WHERE price > 1000000;
SELECT * from items WHERE name like "%watch%";

4.c.
SELECT items.*, categories.name AS kategori from items INNER JOIN categories ON items.category_id = categories.id;

Soal 5

UPDATE items  SET price = 2500000 WHERE id =1;
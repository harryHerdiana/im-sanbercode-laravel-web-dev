<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>OOP</title>
</head>
<body>
    <?php
    require("animal.php");
    require("frog.php");
    require("ape.php");

    $sheep = new Animal("shaun");
    echo ("Name :");
    $sheep->get_name();
    echo ("<br>");
    echo ("Legs :");
    $sheep->get_legs();
    echo ("<br>");
    echo ("Cold Blooded :");
    $sheep->get_cold_blooded();

    echo ("<br>");
    echo ("<br>");

    $kodok = new Frog("buduk");
    echo ("Name :");
    $kodok->get_name();
    echo ("<br>");
    echo ("Legs :");
    $kodok->get_legs();
    echo ("<br>");
    echo ("Cold Blooded :");
    $kodok->get_cold_blooded();
    echo ("<br>");
    echo ("Jump:");
    $kodok->Jump();
    echo ("<br>");

    echo ("<br>");
    echo ("<br>");

    $sungokong = new Ape("kera sakti");
    echo ("Name :");
    $sungokong->get_name();
    echo ("<br>");
    echo ("Legs :");
    $sungokong->get_legs();
    echo ("<br>");
    echo ("Cold Blooded :");
    $sungokong->get_cold_blooded();
    echo ("<br>");
    echo ("Yell :");
    $sungokong->Yell();


    ?>

</body>
</html>
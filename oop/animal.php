<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Animal</title>
</head>
<body>
    <?php
    class Animal
    {
        public $name;
        public function __construct($name)
        {
            $this->name = $name;
        }
        public $legs = 4;
        public $cold_blooded = "no";
        public function get_name()
        {
            echo $this->name;
        }
        public function get_legs()
        {
            echo $this->legs;
        }
        public function get_cold_blooded()
        {
            echo $this->cold_blooded;
        }
    }
    ?>
</body>
</html>